# rebornos-mirrorlist
<br>

**NOTE:** This package loads mirrors from raw, from a location where the list is updated. Therefore, when downloading the repository, before running makepkg, run updpkgsums to update the checksums, and update pkgver. Then compile it.
<br><br>
**IMPORTANT:** Once installed, please run from terminal:

```
sudo update-rmirrors
```
<br><br>

How to clone this repo:

```
git clone https://gitlab.com/rebornos-team/rebornos-special-system-files/rebornos-mirrorlist.git
```

<br><br>
**reborn-mirrorlist raw:**

```
https://gitlab.com/rebornos-team/rebornos-special-system-files/mirrors/reborn-mirrorlist/-/raw/master/reborn-mirrorlist
```

